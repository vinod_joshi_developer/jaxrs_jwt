package com.logicbig.example;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

@Path("/employees")
public class EmployeeResource {

    @GET
    public String getEmployees(@Context SecurityContext securityContext) {
        return "dummy employee list.  user: " + securityContext.getUserPrincipal().getName();
    }

    @GET
    @Path("{id}")
    public String getUser(@PathParam("id") String id,
                          @Context SecurityContext securityContext) {
        return "dummy employee with id: " + id + ". User: " + securityContext.getUserPrincipal();
    }
}